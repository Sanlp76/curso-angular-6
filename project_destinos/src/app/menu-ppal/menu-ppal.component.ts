import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-menu-ppal',
  templateUrl: './menu-ppal.component.html',
  styleUrls: ['./menu-ppal.component.css']
})
export class MenuPpalComponent implements OnInit {
  
  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()), 1000);
    return null;
  });
  constructor(public translate: TranslateService) {
    console.log('***************** get translation');
    translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    translate.setDefaultLang('es');
  }

  public app_name = "Curso Angular"; 
  ngOnInit(): void {
  }

}
