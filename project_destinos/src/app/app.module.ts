import { BrowserModule } from '@angular/platform-browser';
import { NgModule,InjectionToken,Injectable,APP_INITIALIZER } from '@angular/core';
import {RouterModule,Routes} from '@angular/router'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuPpalComponent } from './menu-ppal/menu-ppal.component';
import { ActionReducerMap, StoreModule as NgRxStoreModule,Store } from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './component/login/login.component';
import { ProtectedComponent } from './component/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado.guard';
import { VuelosComponent } from './component/vuelos/vuelos/vuelos.component';
import { VuelosMainComponent } from './component/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './component/vuelos/vuelos-mas-info/vuelos-mas-info.component'
import {VuelosDetalleComponentComponent} from './component/vuelos/vuelos-detalle/vuelos-detalle.component';
import { DestinoDetalleComponent } from './component/destino-detalle/destino-detalle.component';
import { DestinoViajeComponent } from './component/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './component/lista-destinos/lista-destinos.component';
import { FormDestinoViajeComponent } from './component/form-destino-viaje/form-destino-viaje.component'
import { DestinosViajesState,DestinosViajesEffects,intializeDestinosViajesState,reducerDestinosViajes,InitMyDataAction } from './models/destinos-viajes-state.models';
import { NuevoDestinoComponent } from './component/nuevo-destino/nuevo-destino.component';
import { DestinosApiClient } from './models/destinos-api-client.models';
import {TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Observable, from } from 'rxjs';
import { map,flatMap } from 'rxjs/operators';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import Dexie from 'dexie';
import {DestinoViaje} from './models/destino-viaje.models';
import { ReservasDetalleComponent } from './reservas/reservas-detalle/reservas-detalle.component';
import { ReservasListadoComponent } from './reservas/reservas-listado/reservas-listado.component'
import { element } from 'protractor';
//import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { ReservasModule } from './reservas/reservas.module';

// redux init
export interface AppState {
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

const reducersInitialState = {
  destinos: intializeDestinosViajesState()
};

// redux fin init

// app config
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// fin app config

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any>  {
  return () => appLoadService.intializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async intializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

// fin app init

// dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor () {
      super('MyDatabase');
      this.version(1).stores({
        destinos: '++id, nombre, imagenUrl'
      });
      this.version(2).stores({
        destinos: '++id, nombre, imagenUrl',
        translations: '++id, lang, key, value'
      });
  }
}

export const db = new MyDatabase();
// fin dexie db


// i18n ini
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                                        if (results.length === 0) {
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traducciones) => {
                                        console.log('traducciones cargadas:');
                                        console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones) => {
                                        return traducciones.map((t) => ({ [t.key]: t.value}));
                                      });
                      return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
// fin i18n

@NgModule({
  declarations: [
    AppComponent,
    MenuPpalComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponentComponent,
    DestinoDetalleComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    FormDestinoViajeComponent,
    NuevoDestinoComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ReservasModule,  
    NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      }},
      ),
      EffectsModule.forRoot([DestinosViajesEffects]),
      StoreDevtoolsModule.instrument(),      
      NgxMapboxGLModule,
      BrowserAnimationsModule,      
      TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (HttpLoaderFactory),
            deps: [HttpClient]
        }
    }),
    
  ],

  providers: [AuthService,
              UsuarioLogueadoGuard,
              DestinosApiClient,
              MyDatabase,
              UsuarioLogueadoGuard,
              { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
              AppLoadService,
              { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }
              ],              
  bootstrap: [AppComponent]
})
export class AppModule { }
