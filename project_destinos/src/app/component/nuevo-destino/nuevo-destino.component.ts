import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { AppState } from 'src/app/app.module';
import {Store} from '@ngrx/store'
import { DestinoViaje } from 'src/app/models/destino-viaje.models';
import {DestinosApiClient} from 'src/app/models/destinos-api-client.models'
@Component({
  selector: 'app-nuevo-destino',
  templateUrl: './nuevo-destino.component.html',
  styleUrls: ['./nuevo-destino.component.css']
})

export class NuevoDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all: DestinoViaje[];
  
  constructor(public destinosApiClient: DestinosApiClient,private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates= [];
    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if (d != null) {
        this.updates.push('se ha elegido a ' + d.nombre);
      }
    });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
    }

  ngOnInit(): void {
    
  }
  
  agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d: DestinoViaje) {
    this.destinosApiClient.elegir(d);
    }

    getAll(){
      
    }


  }
