import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './component/login/login.component'
import {ProtectedComponent} from './component/protected/protected.component'
import {UsuarioLogueadoGuard} from './guards/usuario-logueado.guard'
import { ListaDestinosComponent } from './component/lista-destinos/lista-destinos.component';
import { FormDestinoViajeComponent } from './component/form-destino-viaje/form-destino-viaje.component';
import { NuevoDestinoComponent } from './component/nuevo-destino/nuevo-destino.component';
import { VuelosComponent } from './component/vuelos/vuelos/vuelos.component';
import { VuelosMasInfoComponent } from './component/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosDetalleComponentComponent } from './component/vuelos/vuelos-detalle/vuelos-detalle.component';
import { DestinoDetalleComponent } from './component/destino-detalle/destino-detalle.component';



const routes: Routes = [
  {path: 'form-nuevo-destino', component: NuevoDestinoComponent},
  { path: 'login', component: LoginComponent },
  {path: 'destino/:id', component:DestinoDetalleComponent},
  //{path: 'destino', component:DestinoDetalleComponent},
  {path: 'protected',component: ProtectedComponent,canActivate: [ UsuarioLogueadoGuard ]},
  //{path: 'vuelos',component: VuelosComponent,canActivate: [ UsuarioLogueadoGuard ],children: childrenRoutesVuelos}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers:[]
})


export class AppRoutingModule {
  
 }



